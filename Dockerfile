FROM golang:latest


RUN mkdir  /data
VOLUME [ "/data" ]

RUN mkdir -p /go/src/resize-photo
WORKDIR /go/src/resize-photo

COPY . .
RUN go get
RUN go build -o resize-photo

ENTRYPOINT [ "/go/src/resize-photo/resize-photo" ]