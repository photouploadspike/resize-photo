package main

import (
	"fmt"
	"log"
	"path/filepath"

	"github.com/disintegration/imaging"
)

// ProcessImage performs some awesome image operation
func ProcessImage(imagePath string) {

	extension := filepath.Ext(imagePath)
	pathPrefix := imagePath[0 : len(imagePath)-len(extension)]

	// Open a test image.
	src, err := imaging.Open(imagePath, imaging.AutoOrientation(true))
	if err != nil {
		log.Fatalf("failed to open image: %v", err)
	}

	// Resize the cropped image to width = 200px preserving the aspect ratio.
	min := imaging.Resize(src, 200, 0, imaging.Lanczos)
	FailOnError(imaging.Save(min, fmt.Sprintf("%s%s%s", pathPrefix, "_min", extension)), "Could not save min")

	img4 := imaging.AdjustContrast(src, 20)
	img4 = imaging.Sharpen(img4, 2)
	FailOnError(imaging.Save(img4, fmt.Sprintf("%s%s%s", pathPrefix, "_sharpened", extension)), "Could not save sharpened")

	// Create an inverted version of the image.
	img3 := imaging.Invert(src)
	FailOnError(imaging.Save(img3, fmt.Sprintf("%s%s%s", pathPrefix, "_inverted", extension)), "Could not save inverted")

	// Create a grayscale version of the image with higher contrast and sharpness.
	img2 := imaging.Grayscale(src)
	img2 = imaging.AdjustContrast(img2, 20)
	img2 = imaging.Sharpen(img2, 2)
	FailOnError(imaging.Save(img2, fmt.Sprintf("%s%s%s", pathPrefix, "_grayscale", extension)), "Could not save grayscale")
}
