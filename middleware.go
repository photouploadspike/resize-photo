package main

import (
	"fmt"
	"os"
	"time"

	"github.com/matryer/try"

	"github.com/streadway/amqp"
)

// Middleware exposes amqp
type Middleware struct {
	channel *amqp.Channel
}

// NewMiddleware creates a middleware
func NewMiddleware() *Middleware {
	var connection *amqp.Connection
	FailOnError(try.Do(func(attempt int) (bool, error) {
		conn, err := amqp.Dial(fmt.Sprintf("amqp://guest:guest@%s:5672/", getAmqpHost()))
		connection = conn

		if err != nil {
			time.Sleep(5 * time.Second)
		}
		return attempt < 5, err
	}), "Failed to connect to RabbitMQ")

	channel, err := connection.Channel()
	FailOnError(err, "Failed to open a channel")

	return &Middleware{channel}
}

// RegisterExchange declares an exchange onto the middleware
func (middleware *Middleware) RegisterExchange(exchangeName string, exchangeType string) error {
	return middleware.channel.ExchangeDeclare(
		exchangeName, // name
		exchangeType, // type
		false,        // durable
		false,        // auto-deleted
		false,        // internal
		false,        // no-wait
		nil,          // arguments
	)
}

// BindQueue Create queue and bind it to exchange with routing key
func (middleware *Middleware) BindQueue(queueName string, routingKey string, exchangeName string) error {

	q, err := middleware.channel.QueueDeclare(
		queueName, // name
		false,     // durable
		false,     // delete when usused
		false,     // exclusive
		false,     // no-wait
		nil,       // arguments
	)
	if err != nil {
		return err
	}
	return middleware.channel.QueueBind(
		q.Name,       // queue name
		routingKey,   // routing key
		exchangeName, // exchange
		false,        // no wait
		nil)          // args
}

// Consume provides chan based on queue consumption
func (middleware *Middleware) Consume(queueName string) (<-chan amqp.Delivery, error) {
	return middleware.channel.Consume(
		queueName, // queue
		"",        // consumer
		true,      // auto-ack
		false,     // exclusive
		false,     // no-local
		false,     // no-wait
		nil,       // args
	)
}

func getAmqpHost() string {
	fromEnv := os.Getenv("AMQP_HOST")
	if len(fromEnv) > 0 {
		return fromEnv
	}
	return "localhost"
}
