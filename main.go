package main

import (
	"fmt"
	"log"

	"github.com/streadway/amqp"

	"io/ioutil"
)

// FailOnError panic if err is not nil
func FailOnError(err error, msg string) {
	if err != nil {
		log.Panicf("%s: %s", msg, err)
	}
}

func main() {
	forever := make(chan bool)

	middleware := NewMiddleware()
	FailOnError(middleware.RegisterExchange("image", "topic"), "Failed to declare an exchange")
	FailOnError(middleware.BindQueue("image-resize", "image-upload", "image"), "Failed to bind queue")

	imageUploaded, err := middleware.Consume("image-resize")
	FailOnError(err, "Failed to register a consumer")

	imageSaved := make(chan string)

	go func() {
		for imageDelivery := range imageUploaded {
			saveImage(imageDelivery, imageSaved)
		}
	}()
	go func() {
		for filepath := range imageSaved {
			ProcessImage(filepath)
		}
	}()

	log.Printf(" [*] Waiting for images. To exit press CTRL+C")

	<-forever
}

func saveImage(delivery amqp.Delivery, onSave chan string) {
	if filename, ok := delivery.Headers["filename"].(string); ok {
		filepath := fmt.Sprintf("/data/%s", filename)
		FailOnError(ioutil.WriteFile(filepath, delivery.Body, 0666), "Could not save image")
		onSave <- filepath
	}
}
