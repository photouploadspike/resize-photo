package main

import (
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFailOnErrorWhenErrorIsNilShouldNotPanic(t *testing.T) {
	assert.NotPanics(t, func() { FailOnError(nil, "some message") })
}

func TestFailOnErrorWhenErrorIsNotNilShouldPanic(t *testing.T) {
	assert.PanicsWithValue(t, "some message: haha", func() { FailOnError(errors.New("haha"), "some message") })
}
