# Goals

Process images uploaded by user.

# Run


## Middleware 

Before running the application, make sure rabbitMq is running and available.
You can change rabbit mq HOST with the environment variable AMQP_HOST.

If the variable is not define, the app falls back to localhost.

## Output

The container will write to volume point /data

## Launch

With node and npm you can :
```sh
go get
go build
./resize-photo
```

With docker you can 
```sh
docker build -t resize-photo .
docker run -d  -e AMQP_HOST=10.0.0.1 -v /some/path:/data resize-photo:latest
```